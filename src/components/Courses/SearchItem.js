import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { StyleSheet, TouchableHighlight } from 'react-native';
import { COLORS } from '../../constants/Colors';
import { FONTS } from '../../constants/Fonts';
import { normalize } from '../../constants/Platform';
import PlText from '../../ui-elements/PlText';
import PlView from '../../ui-elements/PlView';

const SearchItem = props => {
    const {
        title,
        description,
        short
    } = props;
    const navigation = useNavigation();
    return (<TouchableHighlight
        underlayColor={'transparent'}
        onPress={() => navigation.navigate('SearchDetails',{wordInfo:{short:short,long:description,title:title}})}>
        <PlView style={styles.dataContainer}>
            <PlText style={styles.title} numberOfLines={2}>
                {title}
            </PlText>
            {short ? <PlText numberOfLines={2} style={styles.author}>{short}</PlText> : null}
        </PlView>
    </TouchableHighlight>);
};
const styles = StyleSheet.create({
    completedContainer: {
        borderColor: COLORS.PRIMARY,
        borderWidth: 2,
        paddingVertical: 2,
        paddingHorizontal: normalize(8),
        alignItems: 'center',
        marginVertical: normalize(16),
        borderRadius: normalize(16),
        width: '75%',
    },
    completedText: {
        fontFamily: FONTS.SEMI_BOLD,
        color: COLORS.PRIMARY,
        fontSize: normalize(14),
    },
    container: {
        flexDirection: 'row',
        marginHorizontal: normalize(24),
        marginVertical: normalize(12),
    },
    image: {
        width: normalize(80),
        height: normalize(80),
        borderRadius: normalize(8),
        elevation: 5,
        shadowOffset: { width: 3, height: 5 },
        shadowColor: COLORS.LIGHT_GREY,
        shadowOpacity: 1.0,
        shadowRadius: 3,
        borderColor: COLORS.LIGHT_GREY,
    },
    title: {
        fontFamily: FONTS.BOLD,
        fontSize: normalize(14),
        color: COLORS.BLACK,
    },
    author: {
        fontFamily: FONTS.SEMI_BOLD,
        fontSize: normalize(12),
        color: COLORS.GREY,
    },
    description: {
        fontFamily: FONTS.REGULAR,
        fontSize: normalize(12),
        color: COLORS.DARK,
    },
    dataContainer: {
        elevation: 2,
        backgroundColor: COLORS.WHITE,
        marginVertical: normalize(8),
        padding: normalize(6),
        borderRadius:normalize(8),
        justifyContent: 'center',
        marginHorizontal: normalize(16),
    },
});

export default SearchItem;
