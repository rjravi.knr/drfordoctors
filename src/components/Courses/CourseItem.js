import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { StyleSheet, TouchableHighlight, TouchableOpacity } from 'react-native';
import FastImage from 'react-native-fast-image';
import { COLORS } from '../../constants/Colors';
import { FONTS } from '../../constants/Fonts';
import { normalize } from '../../constants/Platform';
import PlProgressBar from '../../ui-elements/PlProgressBar';
import PlText from '../../ui-elements/PlText';
import PlView from '../../ui-elements/PlView';
import PriceTag from '../PriceTag';
import RatingBar from '../RatingBar';

const CourseItem = props => {
  const {
    title,
    author,
    imageUrl,
    rating,
    progress,
    completed,
    onCertificateClick,
    description,
    offerdPrice,
    basePrice,
    currency,
    course,
    learningPaths
  } = props;
  const navigation = useNavigation();
  return (
    <TouchableHighlight
      underlayColor={'transparent'}
      onPress={() => learningPaths ? navigation.navigate('LearningPaths', { info: course }):navigation.navigate('CourseDetail',{info:course})}>
      <PlView style={styles.container}>
        <FastImage style={styles.image} source={{ uri: imageUrl }} />

        <PlView style={styles.dataContainer}>
          <PlText style={styles.title} numberOfLines={2}>
            {title}
          </PlText>
          {author ? <PlText style={styles.author}>{author}</PlText> : null}
          {description ? <PlText numberOfLines={2} style={styles.author}>{description}</PlText> : null}

          <PlView
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: normalize(8),
            }}>
            {offerdPrice || basePrice ? (
              <PriceTag
                offerdPrice={offerdPrice}
                basePrice={basePrice}
                currency={currency}
              />
            ) : null}
            {rating ? <RatingBar stars={5} rating={rating} /> : null}
          </PlView>
          {progress ? (
            <PlProgressBar
              progress={progress}
              message={`${progress} % Completed`}
            />
          ) : null}
          {completed ? (
            <PlView style={styles.completedContainer}>
              <TouchableOpacity onPress={onCertificateClick}>
                <PlText style={styles.completedText}>View certificate</PlText>
              </TouchableOpacity>
            </PlView>
          ) : null}
        </PlView>
      </PlView>
    </TouchableHighlight>
  );
};
const styles = StyleSheet.create({
  completedContainer: {
    borderColor: COLORS.PRIMARY,
    borderWidth: 2,
    paddingVertical: 2,
    paddingHorizontal: normalize(8),
    alignItems: 'center',
    marginVertical: normalize(16),
    borderRadius: normalize(16),
    width: '75%',
  },
  completedText: {
    fontFamily: FONTS.SEMI_BOLD,
    color: COLORS.PRIMARY,
    fontSize: normalize(14),
  },
  container: {
    flexDirection: 'row',
    marginHorizontal: normalize(24),
    marginVertical: normalize(12),
  },
  image: {
    width: normalize(80),
    height: normalize(80),
    borderRadius: normalize(8),
    elevation: 5,
    shadowOffset: { width: 3, height: 5 },
    shadowColor: COLORS.LIGHT_GREY,
    shadowOpacity: 1.0,
    shadowRadius: 3,
    borderColor: COLORS.LIGHT_GREY,
  },
  title: {
    fontFamily: FONTS.BOLD,
    fontSize: normalize(14),
    color: COLORS.BLACK,
  },
  author: {
    fontFamily: FONTS.SEMI_BOLD,
    fontSize: normalize(12),
    color: COLORS.GREY,
  },
  description: {
    fontFamily: FONTS.REGULAR,
    fontSize: normalize(12),
    color: COLORS.DARK,
  },
  dataContainer: {
    flex: 1,
    justifyContent: 'center',
    marginHorizontal: normalize(16),
  },
});

export default CourseItem;
