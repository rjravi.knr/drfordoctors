import _ from 'lodash';
import React from 'react';
import PlView from '../../ui-elements/PlView';
import CourseHeading from './CourseHeading';
import CourseItem from './CourseItem';

const CourseCollection = props => {
  const { title, item, learningPaths } = props;
  return (
    <PlView>
      <CourseHeading title={title} viewAll={ false} />
      {_.map(item, single => {
        return (<CourseItem
          learningPaths={learningPaths}
          rating={single.rating}
          author={single.author && single.author.name || ''}
          title={single.title}
          description={single.description}
          imageUrl={single.thumbnail}
          course={single}
        />);
      })}
    </PlView>
  );
};

export default CourseCollection;
