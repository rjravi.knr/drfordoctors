import auth from '@react-native-firebase/auth';
import { useNavigation } from '@react-navigation/native';
import _ from 'lodash';
import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Toast from 'react-native-toast-message';
import { COLORS } from '../constants/Colors';
import { FONTS } from '../constants/Fonts';
import { normalize } from '../constants/Platform';
import PlIcon from '../ui-elements/PlIcon';
import PlText from '../ui-elements/PlText';
import PlView from '../ui-elements/PlView';

const NavBarContentCard = props => {
  const { items, title, titleIcon, iconType } = props;

  const navigation = useNavigation();
  const signoutFromApplication = () => {
    auth().signOut().then(resp => {
      console.log("SIGNED OUT FROM APPLICATION")
      Toast.show({
        type: 'success',
        text1: 'Success',
        text2: 'Signed out successfully'
      });
      navigation.navigate("TutorailScreen")
    })
  }
  return (
    <PlView style={styles.container}>
      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
        <PlIcon
          style={styles.icon}
          size={16}
          name={titleIcon}
          type={iconType}
        />
        <PlText style={styles.title}>{title.toUpperCase()}</PlText>
      </View>

      {_.map(items, item => {
        return (
          <TouchableOpacity
            style={styles.itemContainer}
            onPress={() => item.value === 'signout' ? signoutFromApplication() : navigation.navigate(item.value)}>
            <Text style={styles.itemText}>{item.label}</Text>
          </TouchableOpacity>
        );
      })}
    </PlView>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f2f2f2',
    marginRight: normalize(24),
    borderTopRightRadius: normalize(16),
    borderBottomRightRadius: normalize(16),
    marginTop: normalize(24),
    paddingVertical: normalize(16),
    paddingHorizontal: normalize(24),
    alignContent: 'center',
  },
  title: {
    marginVertical: normalize(16),
    marginHorizontal: normalize(8),
    fontFamily: FONTS.SEMI_BOLD,
    fontSize: normalize(18),
    color: COLORS.BLACK,
  },
  icon: {
    color: COLORS.GREY,
  },
  itemContainer: {
    marginVertical: normalize(8),
    marginHorizontal: normalize(24),
  },
  itemText: {
    fontFamily: FONTS.REGULAR,
    fontSize: normalize(16),
    color: COLORS.GREY,
  },
});

export default NavBarContentCard;
