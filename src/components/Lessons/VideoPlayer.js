import React from 'react';
import { Platform, StyleSheet } from 'react-native';
import Video from 'react-native-video';
import { COLORS } from '../../constants/Colors';
import { normalize } from '../../constants/Platform';
import PlView from '../../ui-elements/PlView';

const VideoPlayer = (props) => {
  console.log("PROPS", props)
  return (
    <PlView style={styles.videoPlayer}>
      <Video
        resizeMode={'cover'}
        source={{
          uri: props.resource
        }} // Can be a URL or a local file.
        style={{
          position: 'absolute',
          top: 0,
          bottom: 0,
          left: 0,
          right: 0,
        }}
        onVideoProgress={() => console.log("IN PRORO")}
        onVideoLoadStart={() => console.log(">>>>>>LOD STARTED")}
        onVideoLoad={() => console.log("VIDEO LOADED")}
        onError={(err) => console.log("SOME ERROR", err)}
        playInBackground={true}
        pictureInPicture={Platform.OS === 'ios' ? true : false}
        bufferConfig={{
          minBufferMs: 15000,
          maxBufferMs: 50000,
          bufferForPlaybackMs: 2500,
          bufferForPlaybackAfterRebufferMs: 5000
        }}
        onBuffer={() => console.log("BUFFERIN")}
        poster={props.thumbnail}
        posterResizeMode={'cover'}
      />
      {/* <VideoPlayerController /> */}
    </PlView>
  );
};

const styles = StyleSheet.create({
  videoPlayer: {
    width: '100%',
    height: normalize(180),
    backgroundColor: COLORS.BLACK,
  },
});

export default VideoPlayer;
