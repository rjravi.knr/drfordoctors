import React from 'react';
import { StyleSheet } from 'react-native';
import { COLORS } from '../../constants/Colors';
import { FONTS } from '../../constants/Fonts';
import { normalize } from '../../constants/Platform';
import PlIcon from '../../ui-elements/PlIcon';
import PlProgressBar from '../../ui-elements/PlProgressBar';
import PlText from '../../ui-elements/PlText';
import PlView from '../../ui-elements/PlView';

const CardProgress = (props) => {
  return (
    <PlView style={styles.container}>
      <PlIcon
        name={'shuffle'}
        type={'MaterialIcons'}
        size={24}
        style={styles.icon}
      />
      <PlView style={{ flex: 1, marginHorizontal: normalize(16) }}>
        <PlProgressBar progress={props.currentItem * 10} />
      </PlView>
      <PlText style={styles.label}> {`${props.currentItem} of ${props.total}`}</PlText>
    </PlView>
  );
};
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    margin: normalize(8),
  },
  label: {
    fontFamily: FONTS.SEMI_BOLD,
    fontSize: normalize(14),
    color: COLORS.BLACK,
  },
  icon: {
    color: COLORS.BLACK,
  },
});
export default CardProgress;
