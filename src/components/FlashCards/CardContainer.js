// import React from 'react';
// import { StyleSheet } from 'react-native';
// // import CardFlip from 'react-native-card-flip';
// import { normalize } from '../../constants/Platform';
// import PlText from '../../ui-elements/PlText';
// import PlView from '../../ui-elements/PlView';
// // import QuestionCard from './QuestionCard';
// // import AnswerCard from './AnswerCard';

// const CardContainer = props => {
//   return (
//     <PlView style={styles.container}>
//       <PlText>Cards</PlText>
//     </PlView>
//   );
// };

// const styles = StyleSheet.create({
//   container: {
//     margin: normalize(16),
//     justifyContent: 'center',
//     alignItems: 'center',
//   },
//   cardContainer: {
//     width: normalize(300),
//     alignItems: 'center',
//     height: '100%',
//   },
// });

// export default CardContainer;


import React, { Component } from 'react';
import {
  StyleSheet, Text, TouchableOpacity, View
} from 'react-native';
import { COLORS } from '../../constants/Colors';
import { normalize } from '../../constants/Platform';
import CardFlip from './CardFlip';

export default class CardContainer extends Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    console.log("WHAT INSIDE PORPS", this.props)
  }
  render() {
    return (
      <View style={styles.container}>
        <CardFlip style={styles.cardContainer} ref={card => (this.card = card)}>
          <TouchableOpacity
            activeOpacity={1}
            style={[styles.card, styles.card1]}
            onPress={() => this.card.flip()}>
            <Text style={styles.TopLabel}>Question {this.props.indexNumber }</Text>
            <Text style={styles.label}>{this.props.itemKey.question}</Text>
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={1}
            style={[styles.card, styles.card2]}
            onPress={() => this.card.flip()}>

            <Text style={styles.TopLabel}>Answer</Text>
            <Text style={styles.label}>{this.props.itemKey.answer}</Text>
          </TouchableOpacity>
        </CardFlip>
      </View>
    )
  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  cardContainer: {
    width: 320,
    height: 470,
  },
  card: {
    width: 320,
    height: 470,
    backgroundColor: COLORS.PRIMARY,
    borderRadius: 5,
    shadowColor: 'rgba(0,0,0,0.5)',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    alignItems: 'center',
    alignContent: 'center',
    shadowOpacity: 0.5,
  },
  card1: {
    backgroundColor: COLORS.PRIMARY,
    alignItems: 'center',
    justifyContent: 'center',
    alignContent: 'center'
  },
  card2: {
    backgroundColor: COLORS.SECONDARY,
    alignItems: 'center',
    justifyContent: 'center',
    alignContent: 'center'
  },
  label: {
    textAlign: 'center',
    fontSize: normalize(24),
    color: '#ffffff',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  TopLabel: {
    textAlign: 'center',
    fontSize: normalize(12),
    color: '#ffffff',
    position: 'absolute',
    top: 10,
    left:10
  },
});

