import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import { useNavigation } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { COLORS } from '../constants/Colors';
import { FONTS } from '../constants/Fonts';
import { normalize } from '../constants/Platform';
import PlProfileImage from '../ui-elements/PlProfileImage';
import PlView from '../ui-elements/PlView';
import _ from 'lodash';
const HorizontalProfileCard = () => {
  const navigation = useNavigation();
  const currentUser = auth().currentUser;
  console.log(">>>", currentUser)
  const [userInfo, setUserInfo] = useState({});

  useEffect(() => {
    getUserInfo()
  }, [])
  const getUserInfo = () => {
    firestore()
      .collection('Users')
      .doc(currentUser.uid)
      .onSnapshot(documentSnapshot => {
        console.log('User data: ', documentSnapshot.data());
        setUserInfo(documentSnapshot.data())
      });
    console.log("LETS SEE USER INFO", userInfo)
  }



  return (
    <PlView style={styles.container}>
      <PlProfileImage
        source={userInfo.photourl}
        imageStyles={styles.imageStyles}
      />
      <View style={styles.textContainer}>
        <Text style={styles.name}>{ userInfo.name}</Text>

        <Text style={styles.profession}>{_.capitalize(userInfo.type) }</Text>
      </View>
      <TouchableOpacity
        style={styles.button}
        onPress={() => navigation.navigate('Profile')}>
        <Text style={styles.buttonText}>Edit Profile</Text>
      </TouchableOpacity>
    </PlView>
  );
};
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
    marginHorizontal: normalize(18),
    marginVertical: normalize(16),
  },
  imageStyles: {
    borderWidth: 3,
    elevation: 5,
    borderColor: COLORS.WHITE,
    shadowOffset: { width: 3, height: 5 },
    shadowColor: COLORS.GREY,
    shadowOpacity: 1.0,
    shadowRadius: 10,
    backgroundColor: COLORS.LIGHT_GREY,
  },
  textContainer: {
    flex: 1,
    marginHorizontal: normalize(16),
  },
  button: {
    borderRadius: normalize(30),
    borderWidth: 2,
    borderColor: COLORS.PRIMARY,
    paddingHorizontal: normalize(18),
    paddingVertical: normalize(6),
  },
  buttonText: {
    fontSize: normalize(14),
    color: COLORS.PRIMARY,
    fontFamily: FONTS.SEMI_BOLD,
  },
  name: {
    color: COLORS.BLACK,
    fontSize: normalize(16),
    fontFamily: FONTS.BOLD,
  },
  profession: {
    color: COLORS.GREY,
    fontFamily: FONTS.REGULAR,
    fontSize: normalize(14),
  },
});

export default HorizontalProfileCard;
