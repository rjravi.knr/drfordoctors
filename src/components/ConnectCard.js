import React from 'react'
import { StyleSheet, TouchableOpacity } from 'react-native'
import FastImage from 'react-native-fast-image'
import { COLORS } from '../constants/Colors'
import { FONTS } from '../constants/Fonts'
import { normalize } from '../constants/Platform'
import PlText from '../ui-elements/PlText'
import PlView from '../ui-elements/PlView'

export default function ConnectCard(props) {
    const {
        user,
        connected,
        onConnect

    } = props;
    return (
        <PlView style={styles.container}>
            <FastImage style={styles.image} source={{ uri: user.photourl }} />
            <PlView style={styles.dataContainer}>
                <PlText style={styles.title} numberOfLines={2}>
                    {user.name}
                </PlText>
                <TouchableOpacity onPress={onConnect}>
                    <PlText
                        style={styles.button}>
                        {connected ? 'Following' : '+ Follow'}
                    </PlText>
                </TouchableOpacity>

            </PlView>
        </PlView>

    )
}

const styles = StyleSheet.create({
    button: {
        color: COLORS.GREY,
        marginHorizontal: normalize(0),
        marginVertical: normalize(8)
    },
    completedContainer: {
        borderColor: COLORS.PRIMARY,
        borderWidth: 2,
        paddingVertical: 2,
        paddingHorizontal: normalize(8),
        alignItems: 'center',
        marginVertical: normalize(16),
        borderRadius: normalize(16),
        width: '75%',
    },
    completedText: {
        fontFamily: FONTS.SEMI_BOLD,
        color: COLORS.PRIMARY,
        fontSize: normalize(14),
    },
    container: {
        flexDirection: 'row',
        marginHorizontal: normalize(24),
        marginVertical: normalize(12),
    },
    image: {
        width: normalize(60),
        height: normalize(60),
        borderRadius: normalize(30),
        elevation: 5,
        shadowOffset: { width: 3, height: 5 },
        shadowColor: COLORS.LIGHT_GREY,
        shadowOpacity: 1.0,
        shadowRadius: 3,
        borderColor: COLORS.LIGHT_GREY,
    },
    title: {
        fontFamily: FONTS.BOLD,
        fontSize: normalize(16),
        color: COLORS.BLACK,
        fontWeight: 'bold'
    },
    author: {
        fontFamily: FONTS.SEMI_BOLD,
        fontSize: normalize(12),
        color: COLORS.GREY,
    },
    description: {
        fontFamily: FONTS.REGULAR,
        fontSize: normalize(12),
        color: COLORS.DARK,
    },
    dataContainer: {
        flex: 1,
        justifyContent: 'center',
        marginHorizontal: normalize(16),
    },
});
