import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import React from 'react';
import { COLORS } from '../constants/Colors';
import { FONTS } from '../constants/Fonts';
import { dim, normalize } from '../constants/Platform';
import AllCourse from '../screens/MyCourse/AllCourse';
import Following from '../screens/Profile/Following';

const Tab = createMaterialTopTabNavigator();

export default function ProfileTabs() {
  return (
    <Tab.Navigator
      initialRouteName="Info"
      style={{ height: dim().width, marginBottom: normalize(75) }}
      tabBarOptions={{
        indicatorStyle: { backgroundColor: COLORS.PRIMARY },
        activeTintColor: COLORS.PRIMARY,
        inactiveTintColor: COLORS.GREY,
        labelStyle: { fontSize: 14, fontFamily: FONTS.SEMI_BOLD },
        style: {
          marginTop: normalize(16),
          backgroundColor: 'transparent',
          marginHorizontal: normalize(16),
          shadowOffset: { height: 0, width: 0 },
          shadowColor: 'transparent',
          shadowOpacity: 0,
          elevation: 0,
        },
      }}>
      <Tab.Screen
        name="Courses"
        component={AllCourse}
        options={{ tabBarLabel: 'Courses' }}
      />
      <Tab.Screen
        name="Following"
        component={Following}
        options={{ tabBarLabel: 'Following' }}
      />
    </Tab.Navigator>
  );
}
