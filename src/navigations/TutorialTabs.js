import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import React from 'react';
import { COLORS } from '../constants/Colors';
import { FONTS } from '../constants/Fonts';
import { dim, normalize } from '../constants/Platform';
import Info from '../screens/Course/Info';
import Review from '../screens/Course/Review';

const Tab = createMaterialTopTabNavigator();

export default function TutorialTabs(props) {
  const { information } = props
  return (
    <Tab.Navigator
      style={{ height: dim().height, alignContent: 'flex-start' }}
      initialRouteName="Info"
      tabBarOptions={{
        indicatorStyle: { backgroundColor: COLORS.PRIMARY },
        activeTintColor: COLORS.PRIMARY,
        inactiveTintColor: COLORS.GREY,
        labelStyle: { fontSize: 14, fontFamily: FONTS.SEMI_BOLD },
        style: {
          marginTop: normalize(16),
          backgroundColor: 'transparent',
          marginHorizontal: normalize(16),
          shadowOffset: { height: 0, width: 0 },
          shadowColor: 'transparent',
          shadowOpacity: 0,
          elevation: 0,
        },
      }}>
      <Tab.Screen
        name="Info"
        children={() => <Info data={information} />}
        // component={Info}
        options={{ tabBarLabel: 'Info' }}
      />
      <Tab.Screen
        name="medicine"
        children={() => <Review data={information} />}
        // component={Review}
        options={{ tabBarLabel: 'medicine' }}
      />
    </Tab.Navigator>
  );
}
