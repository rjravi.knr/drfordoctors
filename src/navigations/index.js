import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import React, { Component } from 'react';
import MainDrawerNavigation from '../navigations/MainDrawerNavigation';
import CourseDetails from '../screens/CourseDetails';
import FlashCards from '../screens/FlashCards/FlashCards';
import LearningPathCourses from '../screens/LearningPathCourses';
import VideoLesson from '../screens/LessonScreens/VideoLesson';
import Notifications from '../screens/Notifications';
import ProfileScreen from '../screens/ProfileScreen';
import Quiz from '../screens/Quiz/Quiz';
import QuizHome from '../screens/Quiz/QuizHome';
import SearchDetails from '../screens/SearchDetails';
import SwipCardsHome from '../screens/SwipCards/SwipCardsHome';
import SwipeCards from '../screens/SwipCards/SwipeCards';
import TutorialScreen from '../screens/TutorialScreen';
import OTPVerificationScreen from '../screens/User/OtpVerificationScreen';
import SignIn from '../screens/User/Signin';
import SignUp from '../screens/User/SignUp';

const Stack = createStackNavigator();

export default class MainContainer extends Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator
          screenOptions={{
            headerShown: false
          }}
          initialRouteName={'TutorailScreen'}>
          <Stack.Screen name="Main" component={MainDrawerNavigation} />
          <Stack.Screen name="CourseDetail" component={CourseDetails} />
          <Stack.Screen name="LearningPaths" component={LearningPathCourses} />
          <Stack.Screen name="SearchDetails" component={SearchDetails} />
          {/* Video Lessons */}
          <Stack.Screen name="VideoLesson" component={VideoLesson} />
          <Stack.Screen name="TutorailScreen" component={TutorialScreen} />
          {/* user screens */}
          <Stack.Screen name="SignUp" component={SignUp} />
          <Stack.Screen name="SignIn" component={SignIn} />
          <Stack.Screen name="OTPVerification" component={OTPVerificationScreen} />

          <Stack.Screen name="Profile" component={ProfileScreen} />
          {/* FlashCards */}
          <Stack.Screen name="FlashCards" component={FlashCards} />
          {/* Quiz */}
          <Stack.Screen name="Quiz" component={Quiz} />
          <Stack.Screen name="QuizHome" component={QuizHome} />
          {/* SwipeCards */}
          <Stack.Screen name="SwipeCardsHome" component={SwipCardsHome} />
          <Stack.Screen name="SwipeCards" component={SwipeCards} />
          {/* Notifications */}
          <Stack.Screen name="Notification" component={Notifications} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}
