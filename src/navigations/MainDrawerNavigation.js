import {
  createDrawerNavigator,
  DrawerContentScrollView
} from '@react-navigation/drawer';
import * as React from 'react';
import HorizontalProfileCard from '../components/HorizontalProfileCard';
import NavBarContentCard from '../components/NavBarContentCard';
import { COLORS } from '../constants/Colors';
import { FONTS } from '../constants/Fonts';
import { normalize } from '../constants/Platform';
import PlText from '../ui-elements/PlText';
import MainBottomTabNavigator from './MainBottomTabNavigator';

const Drawer = createDrawerNavigator();

function CustomDrawerContent(props) {
  return (
    <DrawerContentScrollView {...props}>
      <HorizontalProfileCard />
      <NavBarContentCard
        title={'content'}
        iconType={'FontAwesome'}
        titleIcon={'folder-open'}
        items={[
          { value: 'FlashCards', label: 'Falsh Cards' },
        ]}
      />
      <NavBarContentCard
        title={'settings'}
        iconType={'MaterialIcons'}
        titleIcon={'settings'}
        items={[{ value: 'signout', label: 'Sign out' }]}
      />
      <PlText style={{
        color: COLORS.BLACK,
        fontFamily: FONTS.SEMI_BOLD,
        fontSize: normalize(18),
        textAlign: 'center',
        marginTop: normalize(36)
      }}>Crafted with ❤️ in 🇮🇳 </PlText>
    </DrawerContentScrollView>
  );
}

export default function MainDrawerNavigation() {
  return (
    <Drawer.Navigator
      screenOptions={{
        headerShown: false,
        drawerStyle: {
          backgroundColor: '#FFF',
          borderTopRightRadius: normalize(30),
          borderBottomRightRadius: normalize(30),
          width: '85%',
        }
      }}
      drawerContent={props => <CustomDrawerContent {...props} />}

      drawerType={'front'}
      drawerContentOptions={{
        itemStyle: { marginVertical: normalize(8) },
      }}>
      <Drawer.Screen
        name="Home"
        component={MainBottomTabNavigator}
      />
    </Drawer.Navigator>
  );
}
