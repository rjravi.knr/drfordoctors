import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import * as React from 'react';
import { COLORS } from '../constants/Colors';
import { FONTS } from '../constants/Fonts';
import FeaturedCourse from '../screens/FeaturedCourse';
import ConnectUser from '../screens/ConnectUser';
import ProfileScreen from '../screens/ProfileScreen';
import SearchDictonary from '../screens/SearchDictonary';
import WishList from '../screens/WishList';
import PlIcon from '../ui-elements/PlIcon';

const Tab = createBottomTabNavigator();

const SELECTED_COLOR = COLORS.PRIMARY;
const DEFAULT_COLOR = COLORS.GREY;

export default function MainBottomTabNavigator() {
  return (
    <Tab.Navigator
      screenOptions={{ headerShown: false }}
      tabBarOptions={{
        activeTintColor: SELECTED_COLOR,
        inactiveTintColor: DEFAULT_COLOR,
        labelStyle: { fontFamily: FONTS.SEMI_BOLD },
      }}>
      <Tab.Screen
        name="Home"
        component={FeaturedCourse}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({ color, size }) => (

            <PlIcon
              type={'FontAwesome5'}
              name={'home'}
              color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Dictonary"
        component={SearchDictonary}
        options={{
          tabBarLabel: 'Dictonary',
          tabBarIcon: ({ color, size }) => (
            <PlIcon
              type={'FontAwesome5'}
              name={'book-medical'}
              color={color} size={size} />
          ),
        }}
      />

      <Tab.Screen
        name="Connect"
        component={ConnectUser}
        options={{
          tabBarLabel: 'Connect',
          tabBarIcon: ({ color, size }) => (
            <PlIcon
              type={'MaterialIcons'}
              name={'connect-without-contact'}
              color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="WishList"
        component={WishList}
        options={{
          tabBarLabel: 'WishList',
          tabBarIcon: ({ color, size }) => (
            <PlIcon
              type={'FontAwesome5'}
              name={'heart'}
              color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Profile"
        component={ProfileScreen}
        options={{
          tabBarLabel: 'Profile',
          tabBarIcon: ({ color, size }) => (
            <PlIcon
              type={'FontAwesome5'}
              name={'user'}
              color={color} size={size} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}
