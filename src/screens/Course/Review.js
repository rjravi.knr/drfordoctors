import _ from 'lodash';
import React from 'react';
import { StyleSheet } from 'react-native';
import { COLORS } from '../../constants/Colors';
import { FONTS } from '../../constants/Fonts';
import { normalize } from '../../constants/Platform';
import PlText from '../../ui-elements/PlText';
import PlView from '../../ui-elements/PlView';

const Review = (props) => {
  console.log("PROPR", props)
  return (
    <PlView style={styles.container}>
      {/* {_.map(ReviewData, item => {
        return (
          <ReviewItem
            name={item.name}
            rating={item.rating}
            review={item.review}
            profile_pic={item.profile_pic}
          />
        );
      })} */}
      {_.map(props.data.Medicine, (text, index) => {
        return <PlText style={styles.content}>{`${index + 1}. ${text}`}</PlText>
      })}

    </PlView>
  );
};
const styles = StyleSheet.create({
  container: {
    margin: normalize(16),
  },
  content: {
    fontFamily: FONTS.REGULAR,
    fontSize: normalize(14),
    margin: normalize(16),
    color: COLORS.BLACK,
    lineHeight: normalize(24)
  },
});
export default Review;
