
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import _ from 'lodash';
import React, { useEffect, useState } from 'react';
import { ScrollView } from 'react-native';
import ConnectCard from '../components/ConnectCard';
import Header from '../components/Header';
import UserListItem from '../components/User/UserListItem';
import PlView from '../ui-elements/PlView';

const ConnectUser = () => {
  const [doctors, setDoctors] = useState([]);
  const [myConnections, setMyConnections] = useState([]);
  const [connectionObjects, setConnectionObjects] = useState([]);
  let currentUser = auth().currentUser;

  useEffect(() => {
    getDoctors();
    getMyConnections();

  }, []);

  const getDoctors = () => {
    firestore()
      .collection('Users')
      .where("type", "==", 'doctor')
      .get()
      .then(querySnapshot => {
        console.log('Total paths LEARNING>: ', querySnapshot.size);
        let users = []
        querySnapshot.forEach(documentSnapshot => {
          users.push(documentSnapshot.data())
          console.log('paths ID: ', documentSnapshot.id, documentSnapshot.data());
        });
        setDoctors(users)
      });
  }

  const getMyConnections = () => {
    firestore()
      .collection('Connect')
      .where("from", "==", currentUser.uid)
      .get()
      .then(querySnapshot => {
        let connections = [];
        let data = []
        console.log('Total paths RECOM>: ', querySnapshot.size);
        querySnapshot.forEach(documentSnapshot => {
          // console.log("<>>>>>",documentSnapshot.id)
          connections.push(documentSnapshot.data().to);
          data.push({ ...documentSnapshot.data(), docId: documentSnapshot.id })
          console.log('paths ID: ', documentSnapshot.id, documentSnapshot.data());
        });
        setConnectionObjects(data)

        setMyConnections(connections);

      });
  }
  const connectDoctor = (dctr_id, status) => {
    let payload = {
      from: currentUser.uid,
      to: dctr_id
    }
    let findRecord = connectionObjects.filter(item=>item.from===currentUser.uid&&item.to===dctr_id)
    console.log("FIND FILTRE",findRecord)
    if (!status) {
      firestore()
        .collection('Connect')
        .add(payload)
        .then(data => {
          getMyConnections()
        });
    } else {
      firestore()
        .collection('Connect')
        .doc(findRecord[0].docId)
        .update({from:currentUser.uid,to:''})
        .then(data => {
          getMyConnections()
        });
    }

  }
  console.log("CONNECTION TOO", myConnections.includes(currentUser.uid));
  console.log("USERS", currentUser)
  return (
    <PlView>
      <Header gradiant title={'Connect'} homeButton />
      <ScrollView>
        {_.map(doctors, single => {
          return (<UserListItem
            disable={false}
            name={single.name}
            profession={_.capitalize(single.type)}
            profile_pic={single.photourl}
            // user={single}
            connected={myConnections.includes(single.id)}
            onConnect={() => connectDoctor(single.id, myConnections.includes(single.id))}
          />);
        })}
      </ScrollView>
    </PlView>
  );
};

export default ConnectUser;
