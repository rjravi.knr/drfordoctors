import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import { useNavigation } from '@react-navigation/native';
import _ from 'lodash';
import React, { useEffect, useState } from 'react';
import { ImageBackground, StyleSheet } from 'react-native';
import RatingBar from '../components/RatingBar';
import UiContainer from '../components/UiContainer';
import { COLORS } from '../constants/Colors';
import { FONTS } from '../constants/Fonts';
import { normalize } from '../constants/Platform';
import TutorialTabs from '../navigations/TutorialTabs';
import PlIcon from '../ui-elements/PlIcon';
import PlText from '../ui-elements/PlText';
import PlView from '../ui-elements/PlView';


export default function CourseDetails({ route }) {
  const { info } = route.params
  let currentUser = auth().currentUser;
  const [userInfo, setUserInfo] = useState({});
  const navigation = useNavigation();
  console.log("INFO IN DETA", info);
  useEffect(() => {
    console.log("DOID CALL AGAIN")
    firestore()
      .collection('Users')
      .doc(currentUser.uid)
      .onSnapshot(documentSnapshot => {
        console.log('User data: ', documentSnapshot.data());
        setUserInfo({ ...documentSnapshot.data() })
      })
  }, [])

  const onStartCourse = () => {
    console.log(":HERE");
    let ongoing = [...userInfo.ongoing, info.docID]
    firestore().collection('Users').doc(currentUser.uid).update({
      ...userInfo,
      ongoing: [...new Set(ongoing)]
    }).then(data => {
      navigation.navigate('VideoLesson', { info })
    })
  }


  console.log("USER INFO", userInfo)
  const addToFavt = () => {
    console.log("<>>>", userInfo.favourite.includes(info.docID))
    if (userInfo && userInfo.favourite && userInfo.favourite.includes(info.docID)) {
      // console.log("IN FIF CONC")
      let removedFilter = _.remove(userInfo.favourite, (item) => item != info.docID)
      console.log("REMOVE DARR", removedFilter)
      firestore().collection('Users').doc(currentUser.uid).update({
        ...userInfo,
        favourite: removedFilter
      }).then(data => {

        setUserInfo({ ...userInfo, favourite: removedFilter })
        console.log("REMOVE", userInfo)
      })
    } else {

      firestore().collection('Users').doc(currentUser.uid).update({
        ...userInfo,
        favourite: [...userInfo.favourite, info.docID]
      }).then(data => {
        setUserInfo({ ...userInfo, favourite: [...userInfo.favourite, info.docID] })
        console.log("ADDED", userInfo)
      })
    }

  }

  return (
    <UiContainer footerButtonPress={onStartCourse} headerGradiant scrollable footerButton={'Start taking Course'} backButton title={'Course'}>
      <ImageBackground
        style={styles.image}
        source={{
          uri: info.thumbnail,
        }}>
        <PlView gradiant transparent style={styles.overlayContainer}>
          <PlText style={styles.title} numberOfLines={2} ellipsizeMode="tail">
            {info.title}
          </PlText>
          <PlView style={styles.overlayFooter}>
            <PlView style={styles.authorContainer}>
              <PlText
                style={styles.authorText}
                numberOfLines={1}
                ellipsizeMode="tail">
                {`${info.author.name}`}
              </PlText>
              <RatingBar stars={5} rating={info.rating} />
            </PlView>
            <PlIcon
              size={30}
              type={'AntDesign'}
              name={userInfo && userInfo.favourite && userInfo.favourite.includes(info.docID) ? 'heart' : 'hearto'}
              style={{
                color: COLORS.WHITE,
                marginHorizontal: normalize(18)
              }}
              onPress={() => addToFavt()}
            />
          </PlView>

        </PlView>
      </ImageBackground>
      <TutorialTabs information={info} />
    </UiContainer>

  )
}

const styles = StyleSheet.create({
  overlayFooter: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  authorContainer: {
    flexDirection: 'column',
    marginHorizontal: normalize(16),
    marginVertical: normalize(8),
    justifyContent: 'space-between',
  },
  image: {
    width: '100%',
    height: normalize(180),
  },
  overlayContainer: {
    width: '100%',
    height: normalize(180),
    paddingVertical: normalize(16),
    justifyContent: 'flex-end',
  },
  title: {
    fontFamily: FONTS.SEMI_BOLD,
    marginHorizontal: normalize(16),
    color: COLORS.WHITE,
    fontSize: normalize(24),
  },
  authorText: {
    fontFamily: FONTS.REGULAR,
    color: COLORS.LIGHT_GREY,
    marginTop: normalize(8),
    fontSize: normalize(14),
  },
});
