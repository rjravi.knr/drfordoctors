import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import _ from 'lodash';
import React, { useEffect, useState } from 'react';
import CourseItem from '../../components/Courses/CourseItem';
import PlView from '../../ui-elements/PlView';

const AllCourse = (props) => {
  const currentUser = auth().currentUser;
  const [userInfo, setUserInfo] = useState({});
  const [onGoingCourses, setOngoingCourses] = useState([]);

  useEffect(() => {
    getUserInfo();
  }, [])
  const getUserInfo = () => {
    firestore()
      .collection('Users')
      .doc(currentUser.uid)
      .onSnapshot(documentSnapshot => {
        console.log('User data: ', documentSnapshot.data());
        setUserInfo(documentSnapshot.data())
        getOnGoingCourses(documentSnapshot.data())
      });
  }
  const getOnGoingCourses = (userData) => {
    console.log("ISER FF", userData && userData.ongoing);
    let ongoingPaths = userData && userData.ongoing || [];
    if (ongoingPaths.length > 0) {
      firestore()
        .collection('Courses')
        .where(firestore.FieldPath.documentId(), 'in', ongoingPaths)
        .onSnapshot(querySnapshot => {
          console.log('COURSES data: ', querySnapshot);

          let courses = []
          querySnapshot.forEach(documentSnapshot => {
            courses.push({ ...documentSnapshot.data(), docID: documentSnapshot.id })
            console.log('FAV paths ID: ', documentSnapshot.id, documentSnapshot.data());
          });
          setOngoingCourses(courses)
        })
    }

  }
  return (
    <PlView>
      {_.map(onGoingCourses, single => {
        return (
          <CourseItem
            rating={single.rating}
            author={single.author && single.author.name || ''}
            title={single.title}
            description={single.description}
            imageUrl={single.thumbnail}
            course={single}
          />
        );
      })}
    </PlView>
  );
};

export default AllCourse;
