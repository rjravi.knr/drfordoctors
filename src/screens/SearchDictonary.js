import firestore from '@react-native-firebase/firestore';
import _ from 'lodash';
import React, { useEffect, useState } from 'react';
import { StyleSheet } from 'react-native';
import SearchItem from '../components/Courses/SearchItem';
import Header from '../components/Header';
import { COLORS } from '../constants/Colors';
import { isIos, normalize } from '../constants/Platform';
import PlTextInput from '../ui-elements/PlTextInput';
import PlView from '../ui-elements/PlView';

export default function SearchDictonary() {
  const [words, setWords] = useState([]);

  useEffect(() => {
    getMedicalTerms();
  }, []);

  const getMedicalTerms = () => {
    firestore()
      .collection('Dictionary')
      .get()
      .then(querySnapshot => {
        console.log('Total paths LEARNING>: ', querySnapshot.size);
        let words = []
        querySnapshot.forEach(documentSnapshot => {
          words.push(documentSnapshot.data())
          console.log('paths ID: ', documentSnapshot.id, documentSnapshot.data());
        });
        setWords(words)
      });
  }
  return (
    <PlView>
      <Header gradiant title={'Search'} homeButton />
      <PlView gradiant>
        <PlTextInput
          containerStyle={styles.inputContainer}
          autoFocus={true}
          iconName={'search'}
          iconType={'MaterialIcons'}
        />
      </PlView>
      {_.map(words, single => {
        return (
          <SearchItem
            description={single.description}
            short={single.short}
            title={single.title}
          />
        );
      })}
    </PlView>
  )
}
const styles = StyleSheet.create({
  inputContainer: {
    backgroundColor: COLORS.WHITE,
    borderRadius: normalize(8),
    paddingVertical: isIos() ? normalize(12) : 0,
  },
});