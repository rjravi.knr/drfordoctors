import firestore from '@react-native-firebase/firestore';
import React, { useEffect, useState } from 'react';
import { ScrollView } from 'react-native';
import CourseCollection from '../components/Courses/CourseCollection';
import Header from '../components/Header';
import { normalize } from '../constants/Platform';
import PlView from '../ui-elements/PlView';

export default function FeaturedCourse() {
  const [learningPaths, setLearningPaths] = useState([]);
  const [recomendedCourses, setRecomendedCourses] = useState([]);

  useEffect(() => {
    getLearningPaths();
    getRecomendedCourses();

  }, []);

  const getLearningPaths = () => {
    firestore()
      .collection('LearningPaths')
      .get()
      .then(querySnapshot => {
        console.log('Total paths LEARNING>: ', querySnapshot.size);
        let courses = []
        querySnapshot.forEach(documentSnapshot => {
          courses.push(documentSnapshot.data())
          console.log('paths ID: ', documentSnapshot.id, documentSnapshot.data());
        });
        setLearningPaths(courses)
      });
  }

  const getRecomendedCourses = () => {
    firestore()
      .collection('Courses')
      .where("recommended", "==", true)
      .get()
      .then(querySnapshot => {
        let courses = []
        console.log('Total paths RECOM>: ', querySnapshot.size);
        querySnapshot.forEach(documentSnapshot => {
          courses.push({ ...documentSnapshot.data(), docID: documentSnapshot.id })
          console.log('paths ID: ', documentSnapshot.id, documentSnapshot.data());
        });

        setRecomendedCourses(courses)
      });
  }

  return (
    <PlView>
      <Header gradiant title={'Courses'} homeButton />
      <ScrollView style={{ marginBottom: normalize(75) }}>
        <CourseCollection
          learningPaths={true}
          title={'Learning Paths'}
          item={learningPaths}
        />
        <CourseCollection title={'Recomended courses'}
          item={recomendedCourses}
        />
      </ScrollView>
    </PlView>
  )
}