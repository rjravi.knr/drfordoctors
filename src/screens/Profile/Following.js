import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import _ from 'lodash';
import { default as React, useEffect, useState } from 'react';
import UserListItem from '../../components/User/UserListItem';
import PlView from '../../ui-elements/PlView';

const Following = () => {
  const currentUser = auth().currentUser;
  console.log(">>>", currentUser)
  const [userInfo, setUserInfo] = useState({});
  const [userFavts, setUserFavts] = useState([]);
  const [userFavtInformation, setUserFavtInformation] = useState([]);

  useEffect(() => {
    getUserInfo();
    getFollowing();
  }, [])
  const getUserInfo = () => {
    firestore()
      .collection('Users')
      .doc(currentUser.uid)
      .onSnapshot(documentSnapshot => {
        console.log('User data: ', documentSnapshot.data());
        setUserInfo(documentSnapshot.data())
      });
  }
  const getFollowing = () => {
    firestore()
      .collection('Connect')
      .where("from", "==", currentUser.uid)
      .get()
      .then(querySnapshot => {
        let favts = []
        console.log('Total paths RECOM>: ', querySnapshot.size);
        querySnapshot.forEach(documentSnapshot => {
          favts.push({ ...documentSnapshot.data(), docID: documentSnapshot.id })
        });
        console.log("FAAVAS AS ", favts.filter(item => item.to != ""))
        let toFavts = favts.filter(item => item.to != "")
        setUserFavts(toFavts.map(item => item.to))
        fetchFavtUsersNow(toFavts.map(item => item.to));
      })
  }
  const fetchFavtUsersNow = (favts) => {
    console.log("SO FAVT NOOOOOOOOOOOOOO", favts)
    firestore()
      .collection('Users')
      .where(firestore.FieldPath.documentId(), 'in', favts)
      .onSnapshot(querySnapshot => {
        console.log('COURSES data: ', querySnapshot);
        let users = []
        querySnapshot.forEach(documentSnapshot => {
          users.push({ ...documentSnapshot.data() })
        });
        setUserFavtInformation(users)
      });
  }

  console.log("SO FAVT USERS", userFavts)

  return (
    <PlView>
      {_.map(userFavtInformation, item => {
        return (
          <UserListItem
            connected={true}
            disable={true}
            name={item.name}
            profession={_.capitalize(item.type)}
            profile_pic={item.photourl}
          />
        );
      })}
    </PlView>
  );
};

export default Following;
