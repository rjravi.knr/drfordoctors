import firestore from '@react-native-firebase/firestore';
import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import Carousel from 'react-native-snap-carousel-v4';
import CardContainer from '../../components/FlashCards/CardContainer';
import CardFooter from '../../components/FlashCards/CardFooter';
import CardProgress from '../../components/FlashCards/CardProgress';
import Header from '../../components/Header';
import { COLORS } from '../../constants/Colors';
import { dim, normalize } from '../../constants/Platform';
import PlView from '../../ui-elements/PlView';

export default class FlashCards extends Component {
  constructor(props) {
    super(props);
    this.state = { data: [], length: 0, currentItem: 1 };
  }

  componentDidMount() {
    firestore()
      .collection('FlashCards')
      .get()
      .then(querySnapshot => {
        let favts = []
        console.log('Total paths RECOM>: ', querySnapshot.size);
        querySnapshot.forEach(documentSnapshot => {
          favts.push({ ...documentSnapshot.data(), docID: documentSnapshot.id })
        });
        console.log("FAAVAS AS ", favts)
        this.setState({
          data: favts,
          length: favts.length
        })
      })
  }

  _carousel = ({ currentIndex }) => {
    console.log("CURRENR IND",)
  }
  _renderItem = ({ item, index }) => {
    console.log("<>>>>>>>", index)
    return <CardContainer itemKey={item} indexNumber={index + 1} />;
  };
  render() {
    return (
      <PlView style={{ flex: 1,backgroundColor:COLORS.WHITE }}>
        <Header gradiant backButton title={'Flash Cards'} />
        <PlView style={{ flex: 0.1, backgroundColor: COLORS.WHITE }}>
          <CardProgress total={this.state.length} currentItem={this.state.currentItem} />
        </PlView>
        <PlView style={{ flex: 1, backgroundColor: COLORS.WHITE }}>
          <Carousel

            ref={c => {
              this._carousel = c;
            }}
            data={this.state.data}
            renderItem={this._renderItem}
            sliderWidth={dim().width}
            itemWidth={dim().width}
            layout={'stack'}
            layoutCardOffset={`16`}
            onScrollIndexChanged={(index) => this.setState({ currentItem: index + 1 })}
          />
        </PlView>
        
      </PlView>
    );
  }
}

const styles = StyleSheet.create({
  progressLayout: {
    marginVertical: normalize(8),
    width: '100%',
    backgroundColor: 'red',
  },
});


