import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import _ from 'lodash';
import React, { useEffect, useState } from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import FastImage from 'react-native-fast-image';
import Header from '../components/Header';
import { COLORS } from '../constants/Colors';
import { FONTS } from '../constants/Fonts';
import { normalize } from '../constants/Platform';
import ProfileTabs from '../navigations/ProfileTabs';
import PlCard from '../ui-elements/PlCard';
import PlText from '../ui-elements/PlText';
import PlView from '../ui-elements/PlView';

const ProfileScreen = () => {
  const currentUser = auth().currentUser;
  console.log(">>>", currentUser)
  const [userInfo, setUserInfo] = useState({});
  const [userFavts, setUserFavts] = useState([]);

  useEffect(() => {
    getUserInfo();
    getFollowing();
  }, [])
  const getUserInfo = () => {
    firestore()
      .collection('Users')
      .doc(currentUser.uid)
      .onSnapshot(documentSnapshot => {
        console.log('User data: ', documentSnapshot.data());
        setUserInfo(documentSnapshot.data())
      });
  }
  const getFollowing = () => {
    firestore()
      .collection('Connect')
      .where("from", "==", currentUser.uid)
      .get()
      .then(querySnapshot => {
        let favts = []
        console.log('Total paths RECOM>: ', querySnapshot.size);
        querySnapshot.forEach(documentSnapshot => {
          favts.push({ ...documentSnapshot.data(), docID: documentSnapshot.id })
        });
        console.log("FAAVAS AS ", favts.filter(item => item.to != ""))
        setUserFavts(favts.filter(item => item.to != ""))
      })
  }
  return (
    <PlView>
      <Header gradiant backButton title={'Profile'} />
      <ScrollView>
        <PlView
          style={{
            flexDirection: 'row',
            margin: normalize(16),
            alignItems: 'center',
          }}>
          <FastImage
            style={styles.user_image}
            source={{ uri: userInfo.photourl }}
          />
          <PlView style={styles.nameContainer}>
            <PlText style={styles.nameText}>{userInfo.name}</PlText>
            <PlText style={styles.professionText}>{_.capitalize(userInfo.type)}</PlText>
          </PlView>

          <FastImage
            style={styles.badge}
            source={{
              uri:
                'https://cdn2.iconfinder.com/data/icons/award-and-reward/128/Golden-badges-star-award-winner-512.png',
            }}
          />
        </PlView>
        <PlView style={styles.cardsContainer}>
          <PlCard style={styles.card}>
            <PlText style={styles.title}>{userInfo && userInfo.ongoing && userInfo.ongoing.length || 0}</PlText>
            <PlText style={styles.subTitle}>Ongoing</PlText>
          </PlCard>
          <PlCard style={styles.card}>
            <PlText style={styles.title}>{userInfo && userInfo.favourite && userInfo.favourite.length || 0}</PlText>
            <PlText style={styles.subTitle}>Wishlist</PlText>
          </PlCard>
          <PlCard style={styles.card}>
            <PlText style={styles.title}>{userFavts.length}</PlText>
            <PlText style={styles.subTitle}>Following</PlText>
          </PlCard>
        </PlView>
        <ProfileTabs />
      </ScrollView>
    </PlView>
  );
};
const styles = StyleSheet.create({
  badge: {
    width: normalize(40),
    height: normalize(40),
  },
  nameContainer: {
    marginLeft: normalize(16),
    flex: 1,
  },
  nameText: {
    fontSize: normalize(18),
    fontFamily: FONTS.BOLD,
    color: COLORS.BLACK,
  },
  professionText: {
    fontSize: normalize(14),
    fontFamily: FONTS.REGULAR,
    color: COLORS.GREY,
  },
  cardsContainer: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: normalize(8),
  },
  title: {
    fontFamily: FONTS.EXTRA_BOLD,
    fontSize: normalize(24),
    color: COLORS.PRIMARY,
  },
  subTitle: {
    fontFamily: FONTS.SEMI_BOLD,
    fontSize: normalize(14),
    color: COLORS.GREY,
  },
  card: {
    flex: 1,
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    padding: normalize(16),
    borderRadius: normalize(8),
    marginVertical: normalize(16),
  },
  user_image: {
    width: normalize(80),
    height: normalize(80),
    borderRadius: normalize(40),
    borderWidth: 2,
    borderColor: COLORS.WHITE,
    alignSelf: 'center',
    backgroundColor: COLORS.WHITE,
    elevation: 5,
    shadowOffset: { width: 3, height: 5 },
    shadowColor: COLORS.LIGHT_GREY,
    shadowOpacity: 1.0,
    shadowRadius: 4,
  },
});

export default ProfileScreen;
