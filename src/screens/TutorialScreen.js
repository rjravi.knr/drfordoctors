import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import { useNavigation } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import { Platform, StyleSheet, TouchableOpacity } from 'react-native';
import FastImage from 'react-native-fast-image';
import Logo from '../components/Logo';
import { COLORS } from '../constants/Colors';
import { FONTS } from '../constants/Fonts';
import { normalize } from '../constants/Platform';
import PlText from '../ui-elements/PlText';
import PlView from '../ui-elements/PlView';


const TutorialScreen = () => {
  const navigation = useNavigation();
  const [userInfo, setUserInfo] = useState(null);

  let currentUser = auth().currentUser;
  useEffect(() => {
    if (currentUser != null) {
      getUserInfo()
    }
  }, [])
  const getUserInfo = () => {
    firestore()
      .collection('Users')
      .doc(currentUser && currentUser.uid)
      .onSnapshot(documentSnapshot => {
        console.log('User data: ', documentSnapshot.data());
        setUserInfo(documentSnapshot.data())
      });
    console.log("LETS SEE USER INFO", userInfo)
  }
  console.log("CURRE", currentUser)
  const handleNavigation = () => {
    if (userInfo) {
      navigation.navigate('Main')
    } else {
      navigation.navigate('SignIn')
    }
  }
  return (
    <PlView style={styles.contanier}>
      <FastImage
        style={styles.image}
        source={require('../../assets/landingpage.png')}
      />
      <Logo text={'Dr. For Doctors'} />
      <PlText style={styles.header}>
        {`Education for \nEvery \nDoctors.`}
      </PlText>
      <TouchableOpacity onPress={() => handleNavigation()}>
        <PlView gradiant style={styles.button}>
          <PlText style={styles.buttonText}>Start learning now</PlText>
        </PlView>
      </TouchableOpacity>
    </PlView>
  );
};
export default TutorialScreen;
const styles = StyleSheet.create({
  contanier: {
    flex: 1,
  },
  button: {
    marginHorizontal: normalize(16),
    borderRadius: normalize(50),
    paddingVertical: normalize(16),
    alignItems: 'center',
  },
  buttonText: {
    fontFamily: FONTS.SEMI_BOLD,
    fontSize: normalize(16),
    color: COLORS.WHITE,
  },
  image: {
    width: '100%',
    height: Platform.OS === 'ios' ? '60%' : '55%',
  },
  header: {
    width: '70%',
    marginHorizontal: normalize(16),
    color: COLORS.BLACK,
    fontFamily: FONTS.SEMI_BOLD,
    fontSize: normalize(28),
    marginBottom: normalize(24),
  },
});
