import React from 'react';
import { StyleSheet } from 'react-native';
import Header from '../components/Header';
import { COLORS } from '../constants/Colors';
import { FONTS } from '../constants/Fonts';
import { normalize } from '../constants/Platform';
import PlText from '../ui-elements/PlText';
import PlView from '../ui-elements/PlView';

export default function SearchDetails({ route }) {
    console.log("ROUTE PARAM", route.params);
    const { title, short, long } = route.params.wordInfo
    return (
        <PlView>
            <Header gradiant title={title} backButton />
            <PlText style={styles.title}>
                {long}
            </PlText>
        </PlView>
    )
}

const styles = StyleSheet.create({
    title: {
        fontFamily: FONTS.REGULAR,
        fontSize: normalize(16),
        color: COLORS.BLACK,
        padding: normalize(16),
        lineHeight: normalize(18)
    },

});
