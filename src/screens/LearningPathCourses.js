// import {FeaturedCoursesData} from '../data/FeaturedCoursesData';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import { useNavigation } from '@react-navigation/native';
import _ from 'lodash';
import React, { useEffect, useState } from 'react';
import CourseItem from '../components/Courses/CourseItem';
import Header from '../components/Header';
import PlView from '../ui-elements/PlView';

export default function LearningPathCourses({ route }) {
    const { info } = route.params;
    console.log("LEARN PATH INFOFOFOF", info)
    let currentUser = auth().currentUser;
    const [userInfo, setUserInfo] = useState({});
    const navigation = useNavigation();
    const [favCourses, setFavCourses] = useState([])
    useEffect(() => {
        console.log("DOID CALL AGAIN")
        firestore()
            .collection('Users')
            .doc(currentUser.uid)
            .onSnapshot(documentSnapshot => {
                console.log('User data: ', documentSnapshot.data());
                setUserInfo({ ...documentSnapshot.data() })

                getMyCourses()

            })
    }, [])

    const getMyCourses = () => {
        let favPaths = info.course || [];
        if (favPaths.length > 0) {
            firestore()
                .collection('Courses')
                .where(firestore.FieldPath.documentId(), 'in', favPaths)
                .onSnapshot(querySnapshot => {
                    console.log('COURSES data: ', querySnapshot);

                    let courses = []
                    querySnapshot.forEach(documentSnapshot => {
                        courses.push({ ...documentSnapshot.data(), docID: documentSnapshot.id })
                        console.log('FAV paths ID: ', documentSnapshot.id, documentSnapshot.data());
                    });
                    setFavCourses(courses)
                })
        }

    }

    return (
        <PlView>
            <Header gradiant title={info.title} backButton />
            {_.map(favCourses, single => {
                return (
                    <CourseItem
                        rating={single.rating}
                        author={single.author && single.author.name || ''}
                        title={single.title}
                        description={single.description}
                        imageUrl={single.thumbnail}
                        course={single}
                    />
                );
            })}
        </PlView>
    )
}
