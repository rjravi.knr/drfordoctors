import React, { Component } from 'react';
import {
  Text,
  StyleSheet, TouchableOpacity,
  PermissionsAndroid, Platform
} from 'react-native';
import VideoPlayer from '../../components/Lessons/VideoPlayer';
import UiContainer from '../../components/UiContainer';
import { COLORS } from '../../constants/Colors';
import { FONTS } from '../../constants/Fonts';
import { normalize } from '../../constants/Platform';
import { ParagraphContent } from '../../data/ParagraphContent';
import PlText from '../../ui-elements/PlText';
import PlView from '../../ui-elements/PlView';
import RNFetchBlob from 'rn-fetch-blob'
import Toast from 'react-native-toast-message';

export default function VideoLesson({ route }) {
  console.log("PARAMS FOR VIDE", route.params)
  const { info } = route.params;

  const checkPermission = async () => {

    if (Platform.OS === 'ios') {
      downloadFile();
    } else {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'Storage Permission Required',
            message:
              'Application needs access to your storage to download File',
          }
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          // Start downloading
          downloadFile();
          console.log('Storage Permission Granted.');
        } else {
          // If permission denied then show alert
          console.log('Error', 'Storage Permission Not Granted');
          Toast.show({
            type: 'error',
            text1: 'Error!!',
            text2: 'Storage Permission Not Granted',
            position: 'bottom'
          });
        }
      } catch (err) {
        // To handle permission related exception
        console.log("++++" + err);
        Toast.show({
          type: 'error',
          text1: 'Error!!',
          text2: err,
          position:'bottom'
        });
      }
    }
  };
  const getFileExtention = fileUrl => {
    // To get the file extension
    return /[.]/.exec(fileUrl) ?
      /[^.]+$/.exec(fileUrl) : undefined;
  };

  const downloadFile = () => {

    // Get today's date to add the time suffix in filename
    let date = new Date();
    // File URL which we want to download
    let FILE_URL = info.resource;
    // Function to get extention of the file url
    let file_ext = getFileExtention(FILE_URL);

    file_ext = '.' + file_ext[0];

    // config: To get response by passing the downloading related options
    // fs: Root directory path to download
    const { config, fs } = RNFetchBlob;
    let RootDir = fs.dirs.PictureDir;
    let options = {
      fileCache: true,
      addAndroidDownloads: {
        path:
          RootDir +
          '/file_' +
          Math.floor(date.getTime() + date.getSeconds() / 2) +
          file_ext,
        description: 'downloading file...',
        notification: true,
        // useDownloadManager works with Android only
        useDownloadManager: true,
      },
    };
    config(options)
      .fetch('GET', FILE_URL)
      .then(res => {
        // Alert after successful downloading
        console.log('res -> ', JSON.stringify(res));
        console.log('File Downloaded Successfully.');
        Toast.show({
          type: 'success',
          text1: 'Downloaded!!',
          text2: 'File Downloaded Successfully.',
          position: 'bottom'
        });
      });
  };

  return (
    <UiContainer
      headerGradiant
      backButton
      scrollable
      noFooter
      // footerButton={'Mark as completed'}
      title={info.title}>
      <VideoPlayer resource={info.resource} thumbnail={info.thumbnail} />
      <PlView style={{ flex: 1, flexDirection: 'row' }}>

        <PlText style={styles.title}>
          {info.title}
        </PlText>

        <TouchableOpacity
          style={styles.button}
          onPress={checkPermission}>
          <Text style={styles.text}>
            Download File
          </Text>
        </TouchableOpacity>
      </PlView>
      <PlText style={styles.subTitle}>{`by ${info.author.name}`}</PlText>
      <PlText style={styles.content}>{info.shortDescription}</PlText>
      <PlText style={styles.content}>{info.description}</PlText>
    </UiContainer>

  )
}
const styles = StyleSheet.create({
  text: {
    color: COLORS.WHITE,
    fontSize: 16,
    textAlign: 'center',
    padding: 6,
  },
  button: {
    // width: '80%',
    padding: 4,
    borderRadius:4,
    backgroundColor: COLORS.PRIMARY,
    margin: 10,
  },
  title: {
    fontFamily: FONTS.SEMI_BOLD,
    fontSize: normalize(24),
    color: COLORS.BLACK,
    flex: 1,
    marginHorizontal: normalize(16),
    marginVertical: normalize(8),
  },
  subTitle: {
    fontFamily: FONTS.SEMI_BOLD,
    fontSize: normalize(14),
    color: COLORS.GREY,
    marginHorizontal: normalize(16),
  },
  content: {
    fontFamily: FONTS.REGULAR,
    fontSize: normalize(16),
    color: COLORS.BLACK,
    margin: normalize(16),
  },
});
