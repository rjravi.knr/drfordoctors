import auth from '@react-native-firebase/auth';
import { useNavigation } from '@react-navigation/native';
import React, { useState } from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import Header from '../../components/Header';
import Logo from '../../components/Logo';
import { COLORS } from '../../constants/Colors';
import { FONTS } from '../../constants/Fonts';
import { normalize } from '../../constants/Platform';
import PlButton from '../../ui-elements/PlButton';
import PlCard from '../../ui-elements/PlCard';
import PlText from '../../ui-elements/PlText';
import PlTextInput from '../../ui-elements/PlTextInput';
import PlView from '../../ui-elements/PlView';
import Toast from 'react-native-toast-message';

const SignIn = () => {
  const navigation = useNavigation();
  const [confirm, setConfirm] = useState(null);

  const [code, setCode] = useState('');

  const [phoneNumber, setPhoneNumber] = useState('');

  // Handle the button press
  async function signInWithPhoneNumber() {
    console.log("<>>>", '+91' + phoneNumber)
    if (phoneNumber != '') {
      const confirmation = await auth().signInWithPhoneNumber('+91' + phoneNumber).then(resp => {
        console.log("<SAD>AS</SAD>", resp)
        Toast.show({
          type: 'success',
          text1: 'Success',
          text2: 'OTP Sent Successfully'
        });
        navigation.navigate('OTPVerification', { confirm: resp })
      }).catch(err => {
        Toast.show({
          type: 'error',
          text1: 'Sorry!!',
          text2:'Try after some time'
          
        });
        console.log("ERRR",err)
      });
      // setConfirm(confirmation);

    } else {
      
    }
  }

  async function confirmCode() {
    try {
      await confirm.confirm(code);
    } catch (error) {
      console.log('Invalid code.');
    }
  }
  return (
    <PlView gradiant style={styles.container}>
      <Header headerStyle={{ backgroundColor: 'transparent' }} />
      <Logo text={'Dr. For Doctors'} white />
      <PlCard style={styles.cardContainer}>
        <PlText style={styles.heading}>Welcome</PlText>
        <PlView style={styles.inputContainer}>
          <PlTextInput maxLength={10} onChangeText={text => setPhoneNumber(text)} rightText={'+91'} keyboardType={'phone-pad'} placeholder={'Phone Number'} style={styles.inputField} />
        </PlView>
        <PlButton
          onPress={() => signInWithPhoneNumber()}
          style={styles.button}>
          Send OTP
        </PlButton>
        <TouchableOpacity onPress={() => navigation.navigate('Main')}>
          <PlText style={styles.forgotPassword}>Resend OTP</PlText>
        </TouchableOpacity>
      </PlCard>
    </PlView>
  );
};

const styles = StyleSheet.create({
  forgotPassword: {
    fontFamily: FONTS.SEMI_BOLD,
    fontSize: normalize(14),
    color: COLORS.BLACK,
    alignSelf: 'center',
    textDecorationLine: 'underline',
    marginVertical: normalize(24),

  },
  loginHereText: {
    fontFamily: FONTS.SEMI_BOLD,
    fontSize: normalize(16),
    color: COLORS.WHITE,
    alignSelf: 'center',
    textDecorationLine: 'underline',
  },
  alreadyAccount: {
    fontFamily: FONTS.REGULAR,
    fontSize: normalize(14),
    color: COLORS.WHITE,
    alignSelf: 'center',
  },
  subView: {
    alignItems: 'center',
  },
  inputField: {
    borderBottomWidth: 1,
    flex: 1, color: COLORS.BLACK,
    borderBottomColor: COLORS.LIGHT_GREY,
  },
  inputContainer: {
    marginVertical: normalize(16),
  },
  button: {
    marginHorizontal: normalize(16),
    marginVertical: normalize(8),
  },
  heading: {
    fontFamily: FONTS.EXTRA_BOLD,
    fontSize: normalize(24),
    color: COLORS.BLACK,
    alignSelf: 'center',
    marginTop: normalize(16),
  },
  subHeading: {
    fontFamily: FONTS.SEMI_BOLD,
    fontSize: normalize(16),
    color: COLORS.GREY,
    alignSelf: 'center',
  },
  container: {
    flex: 1,
    alignItems: 'center',
  },
  cardContainer: {
    width: '85%',
    borderRadius: normalize(16),
    marginHorizontal: normalize(16),
    marginVertical: normalize(16),
    padding: normalize(16),
  },
});
export default SignIn;
