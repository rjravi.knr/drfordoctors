import firestore from '@react-native-firebase/firestore';
import { useNavigation } from '@react-navigation/native';
import React, { useState } from 'react';
import { StyleSheet } from 'react-native';
import Toast from 'react-native-toast-message';
import Header from '../../components/Header';
import Logo from '../../components/Logo';
import { COLORS } from '../../constants/Colors';
import { FONTS } from '../../constants/Fonts';
import { normalize } from '../../constants/Platform';
import PlButton from '../../ui-elements/PlButton';
import PlCard from '../../ui-elements/PlCard';
import PlText from '../../ui-elements/PlText';
import PlTextInput from '../../ui-elements/PlTextInput';
import PlView from '../../ui-elements/PlView';

const OTPVerificationScreen = ({ route }) => {
    const navigation = useNavigation();

    // console.log("PARAMS", params);
    const [code, setCode] = useState()
    const { confirm } = route && route.params;

    async function confirmCode() {
        try {
            console.log("CONFIRM CODE", confirm)
            let data = await confirm.confirm(code);
            console.log("CONFIR", data)
            Toast.show({
                type: 'success',
                text1: 'Success',
                text2: 'OTP Verified Successfully'
            });
            handleNavigation(data && data.user)
            // navigation.navigate('SignUp',{userinfo:data})
        } catch (error) {
            Toast.show({
                type: 'error',
                text1: 'Invalid',
                text2: JSON.stringify(error)
            });
            console.log('Invalid code. lets check', error);
        }
    }
    const handleNavigation = (data) => {
        console.log("DATA >UI", data.uid)
        firestore()
            .collection('Users')
            .doc(data.uid)
            .onSnapshot(documentSnapshot => {
                console.log('User data: ', documentSnapshot && documentSnapshot.data());
                if (documentSnapshot && documentSnapshot.data()) {
                    navigation.navigate('Main')
                } else {
                    navigation.navigate('SignUp')
                }
            });

    }
    return (
        <PlView gradiant style={styles.container}>
            <Header headerStyle={{ backgroundColor: 'transparent' }} />
            <Logo text={'Dr. For Doctors'} white />
            <PlCard style={styles.cardContainer}>
                <PlText style={styles.heading}>OTP</PlText>
                <PlView style={styles.inputContainer}>
                    <PlTextInput onChangeText={text => setCode(text)} maxLength={6} keyboardType={'numeric'} placeholder={'Enter 6 digit Otp'} style={styles.inputField} />
                </PlView>
                <PlButton
                    onPress={() => confirmCode()}
                    style={styles.button}>
                    Submit
                </PlButton>

            </PlCard>
        </PlView>
    );
};

const styles = StyleSheet.create({
    forgotPassword: {
        fontFamily: FONTS.SEMI_BOLD,
        fontSize: normalize(14),
        color: COLORS.BLACK,
        alignSelf: 'center',
        textDecorationLine: 'underline',
        marginVertical: normalize(24),

    },
    loginHereText: {
        fontFamily: FONTS.SEMI_BOLD,
        fontSize: normalize(16),
        color: COLORS.WHITE,
        alignSelf: 'center',
        textDecorationLine: 'underline',
    },
    alreadyAccount: {
        fontFamily: FONTS.REGULAR,
        fontSize: normalize(14),
        color: COLORS.WHITE,
        alignSelf: 'center',
    },
    subView: {
        alignItems: 'center',
    },
    inputField: {
        borderBottomWidth: 1,
        flex: 1, color: COLORS.BLACK,
        borderBottomColor: COLORS.LIGHT_GREY,
    },
    inputContainer: {
        marginVertical: normalize(16),
    },
    button: {
        marginHorizontal: normalize(16),
        marginVertical: normalize(8),
    },
    heading: {
        fontFamily: FONTS.EXTRA_BOLD,
        fontSize: normalize(24),
        color: COLORS.BLACK,
        alignSelf: 'center',
        marginTop: normalize(16),
    },
    subHeading: {
        fontFamily: FONTS.SEMI_BOLD,
        fontSize: normalize(16),
        color: COLORS.GREY,
        alignSelf: 'center',
    },
    container: {
        flex: 1,
        alignItems: 'center',
    },
    cardContainer: {
        width: '85%',
        borderRadius: normalize(16),
        marginHorizontal: normalize(16),
        marginVertical: normalize(16),
        padding: normalize(16),
    },
});
export default OTPVerificationScreen;
