import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import { useNavigation } from '@react-navigation/native';
import React, { useState } from 'react';
import { StyleSheet } from 'react-native';
import Header from '../../components/Header';
import Logo from '../../components/Logo';
import { COLORS } from '../../constants/Colors';
import { FONTS } from '../../constants/Fonts';
import { normalize } from '../../constants/Platform';
import PlButton from '../../ui-elements/PlButton';
import PlCard from '../../ui-elements/PlCard';
import PlText from '../../ui-elements/PlText';
import PlTextInput from '../../ui-elements/PlTextInput';
import PlView from '../../ui-elements/PlView';
import Toast from 'react-native-toast-message';

const SignUp = ({ }) => {
  const navigation = useNavigation();
  let currentUser = auth().currentUser;
  const [state, setState] = useState({
    id: currentUser && currentUser.uid,
    address: '',
    age: 0,
    college: '',
    email: '',
    name: '',
    fcmtoken: '',
    favourite: [],
    ongoing:[],
    phone: currentUser && currentUser.phoneNumber,
    photourl: '',
    type: 'student',
    settings: {
      lastlogin: new Date(),
      theme: 'light'
    }

  });

  const registerUser = () => {
    console.log("REGISTER USER", state)
    if (state.name === '') {
      Toast.show({
        type: 'error',
        text1: 'Invalid',
        text2:'Enter your name'
      });
      return
    }
    if (state.email === '') {
      Toast.show({
        type: 'error',
        text1: 'Invalid',
        text2: 'Enter your email'
      });
      return
    }
    if (state.age === 0) {
      Toast.show({
        type: 'error',
        text1: 'Invalid',
        text2: 'Enter your age'
      });
      return
    }
    if (state.college === '') {
      Toast.show({
        type: 'error',
        text1: 'Invalid',
        text2: 'Enter college name'
      });
      return
    }
    
    firestore()
      .collection('Users')
      .doc(state.id)
      .set(state)
      .then(() => {
        console.log('user added!', state);
        navigation.navigate('Main')
      }).catch(err => console.log("ERRORRO", err));

  }
  return (
    <PlView gradiant style={styles.container}>
      <Header headerStyle={{ backgroundColor: 'transparent' }} />
      <Logo text={'Dr. For Doctors'} white />
      <PlCard style={styles.cardContainer}>
        <PlText style={styles.heading}>Register</PlText>
        <PlText style={styles.subHeading}>Create your Dr. For Doctors account</PlText>

        <PlView style={styles.inputContainer}>
          <PlTextInput onChangeText={value => setState({ ...state, name: value })} placeholder={'Your Full Name'} style={styles.inputField} />
          <PlTextInput onChangeText={value => setState({ ...state, email: value })} placeholder={'Email'} style={styles.inputField} />
          <PlTextInput onChangeText={value => setState({ ...state, age: value })} keyboardType={'numeric'} placeholder={'Age'} maxLength={2} style={styles.inputField} />
          <PlTextInput onChangeText={value => setState({ ...state, college: value })} placeholder={'College Name'} style={styles.inputField} />

        </PlView>
        <PlButton
          onPress={() => registerUser()}
          style={styles.button}>
          Create account
        </PlButton>
      </PlCard>

    </PlView>
  );
};

const styles = StyleSheet.create({
  loginHereText: {
    fontFamily: FONTS.SEMI_BOLD,
    fontSize: normalize(16),
    color: COLORS.WHITE,
    alignSelf: 'center',
    textDecorationLine: 'underline',
  },
  alreadyAccount: {
    fontFamily: FONTS.REGULAR,
    fontSize: normalize(14),
    color: COLORS.WHITE,
    alignSelf: 'center',
  },
  subView: {
    alignItems: 'center',
  },
  inputField: {
    borderBottomWidth: 1,
    borderBottomColor: COLORS.LIGHT_GREY,
    flex: 1, color: COLORS.BLACK,
  },
  inputContainer: {
    marginVertical: normalize(8),
  },
  button: {
    marginHorizontal: normalize(16),
    marginVertical: normalize(8)
  },
  heading: {
    fontFamily: FONTS.EXTRA_BOLD,
    fontSize: normalize(24),
    color: COLORS.BLACK,
    alignSelf: 'center',
    marginTop: normalize(8),
  },
  subHeading: {
    fontFamily: FONTS.SEMI_BOLD,
    fontSize: normalize(16),
    color: COLORS.GREY,
    alignSelf: 'center',
  },
  container: {
    flex: 1,
    alignItems: 'center',
  },
  cardContainer: {
    width: '85%',
    borderRadius: normalize(16),
    marginHorizontal: normalize(16),
    marginVertical: normalize(16),
    padding: normalize(8),
  },
});
export default SignUp;
