import React, { Component } from 'react';
import { Provider as PaperProvider } from 'react-native-paper';
import Toast from 'react-native-toast-message';
import MainContainer from './src/navigations/index';

export default class App extends Component {
  render() {
    return <PaperProvider>
      <MainContainer />
      <Toast />
    </PaperProvider>;
  }
}
